library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.brick_types.all;

entity Ball is
port(
  clk        : in std_logic;
  reset_l    : in std_logic;
  go         : in std_logic;
  x          : in natural;
  y          : in natural;
  brick_is   : in std_logic;
  brick_edge : in edge_type;
  brick_x    : in natural;
  brick_y    : in natural;
  paddle_is  : in std_logic;
  paddle_x   : in integer;
  x_out      : out natural := 320;
  y_out      : out natural := 320;
  is_ball    : out std_logic := '0';
  r          : out std_logic_vector(3 downto 0);
  g          : out std_logic_vector(3 downto 0);
  b          : out std_logic_vector(3 downto 0)
);
end Ball;

architecture Behavioral of Ball is
  signal ball_x, ball_y                  : natural := 320;
  signal next_ball_x, next_ball_y        : natural;
  signal dir_y, next_dir_y, next_is_ball : std_logic := '0';
  signal dead                            : std_logic := '1';
  signal m_x, m_y                        : natural := 0;
  signal count, next_count               : natural := 0;
  signal x_speed, next_x_speed, p_x      : integer := 0;
  signal ball_count, next_ball_count     : integer := 5;
begin
  next_ball_x <= ball_x + x_speed;
  next_ball_y <= ball_y + 1 when dir_y = '1' else ball_y - 1;
  next_count <= count+1;
  p_x <= paddle_x + ball_x - x;
  m_x <= x when rising_edge(clk) else m_x;
  m_y <= y when rising_edge(clk) else m_y;

  BounceProc : process (brick_edge, brick_is, ball_y, ball_x, dir_y, next_x_speed, paddle_x, paddle_is)
  begin
    if ball_y < 5 then
      next_dir_y <= '1';
      next_x_speed <= x_speed;
    elsif ball_x < 5 then
      next_dir_y <= dir_y;
      next_x_speed <= 1;
    elsif ball_x > 634 then
      next_dir_y <= dir_y;
      next_x_speed <= -1;
    elsif paddle_is = '1' then
      next_dir_y <= '0';
      if p_x < -7 then
        next_x_speed <= -1;
      elsif p_x > 7 then
        next_x_speed <= 1;
      else
        next_x_speed <= 0;
      end if;
    elsif brick_is then
      case brick_edge is
        when none =>
          next_x_speed <= 0;
          next_dir_y <= '1';
        when top =>
          next_x_speed <= x_speed;
          next_dir_y <= '0';
        when bottom =>
          next_x_speed <= x_speed;
          next_dir_y <= '1';
        when left =>
          next_x_speed <= -1;
          next_dir_y <= dir_y;
        when right =>
          next_x_speed <= 1;
          next_dir_y <= dir_y;
        when topleft =>
          next_x_speed <= -1;
          next_dir_y <= '0';
        when topright =>
          next_x_speed <= 1;
          next_dir_y <= '0';
        when bottomleft =>
          next_x_speed <= -1;
          next_dir_y <= '1';
        when bottomright =>
          next_x_speed <= 1;
          next_dir_y <= '1';
      end case;
    else
      next_dir_y <= dir_y;
      next_x_speed <= x_speed;
    end if;
  end process;

  ClkGameProc : process (clk, reset_l) is
  begin
    if reset_l = '0' then
      dead <= '1';
      ball_count <= 5;
    elsif rising_edge(clk) then
      if ball_y > 475 then
        dead <= '1';
        next_ball_count <= ball_count-1;
      elsif dead = '0'  or ( go = '1' and dead = '1' and ball_count > 0 ) then
        dead <= '0';
        next_ball_count <= ball_count;
      else
        dead <= '1';
        next_ball_count <= ball_count;
      end if;
    end if;
  end process;

  ClkMoveProc : process (clk, reset_l) is
  begin
    if reset_l = '0' then
      is_ball <= '0';
      count <= 0;
      ball_y <= 320;
      ball_x <= 320;
      x_out <= 0;
      x_out <= 0;
    elsif rising_edge(clk) then
      x_out <= m_x;
      y_out <= m_y;
      is_ball <= next_is_ball;
      if ball_y > 475 or dead ='1' then
        count <= 0;
        ball_y <= 320;
        ball_x <= 320;
      elsif (x_speed = 0 and count > 353553) or (x_speed /= 0 and count > 500000) then
        count <= 0;
        ball_y <= next_ball_y;
        ball_x <= next_ball_x;
      else
        count <= next_count;
      end if;
    end if;
  end process;

  ClkSpeedProc : process (clk, reset_l) is
  begin
    if reset_l = '0' then
      dir_y <= '1';
      x_speed <= 0;
    elsif rising_edge(clk) then
      if ball_y > 475 or dead='1' then
        dir_y <= '1';
        x_speed <= 0;
      else
        dir_y <= next_dir_y;
        x_speed <= next_x_speed;
      end if;
    end if;
  end process;

  IsBallProc : process (m_x, m_y, ball_x, ball_y) is
  begin
    if m_y<ball_y+5 and m_y>=ball_y-5 and m_x<ball_x+5  and m_x>=ball_x-5 then
      next_is_ball <= '1';
    else
      next_is_ball <= '0';
    end if;
  end process;

  PixelColorProc : process (x, y, ball_x, ball_y) is
  begin
    if y<ball_y+5  and y>=ball_y-5 and x<ball_x+5  and x>=ball_x-5 and dead='0' then
      r <= x"F";
      g <= x"F";
      b <= x"F";
    else
      r <= x"0";
      g <= x"0";
      b <= x"0";
    end if;
  end process;

end Behavioral;
