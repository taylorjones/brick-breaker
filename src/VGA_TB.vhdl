library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity VGA_TB is
end entity VGA_TB;

architecture Behavioral of VGA_TB is
  component BrickBreaker
  port(
  	MAX10_CLK1_50 : in std_logic;
  	VGA_CLK_25 : in std_logic;
  	ADC_CLK_10 : in std_logic;
  	KEY : in std_logic_vector(1 downto 0);
  	CLK_LOCKED : in std_logic;
  	VGA_B : out std_logic_vector(3 downto 0);
  	VGA_G : out std_logic_vector(3 downto 0);
  	VGA_R : out std_logic_vector(3 downto 0);
  	VGA_HS : out std_logic;
  	VGA_VS : out std_logic
  );
  end component BrickBreaker;

  signal MAX10_CLK1_50, ADC_CLK_10, VGA_CLK_25, CLK_LOCKED  : std_logic := '0';
  signal KEY : std_logic_vector(1 downto 0) := (others => '1');

begin
  uut : BrickBreaker
    port map (
  	 MAX10_CLK1_50 => MAX10_CLK1_50,
   	 ADC_CLK_10 => ADC_CLK_10,
   	 VGA_CLK_25 => VGA_CLK_25,
   	 CLK_LOCKED => CLK_LOCKED,
   	 KEY => KEY
    );
    clk_process_50 : process
    begin
      MAX10_CLK1_50 <= '0';
      wait for 10 ns;
      MAX10_CLK1_50 <= '1';
      wait for 10 ns;
    end process clk_process_50;

    clk_process_25 : process
    begin
      VGA_CLK_25 <= '0';
      wait for 20 ns;
      VGA_CLK_25 <= '1';
      wait for 20 ns;
    end process clk_process_25;

    clk_process_10 : process
    begin
      ADC_CLK_10 <= '0';
      wait for 50 ns;
      ADC_CLK_10 <= '1';
      wait for 50 ns;
    end process clk_process_10;

    stm_process : process --stimulation process
    begin
      KEY <= "00";
      CLK_LOCKED <= '0';
      wait for 40 ns;
      KEY <= "11";
      CLK_LOCKED <= '1';
      wait;
    end process stm_process;
end architecture Behavioral;
