package brick_types is
  type edge_type is (none, top, bottom, left, right, topleft, topright, bottomleft, bottomright);
end brick_types;

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.brick_types.all;

entity Bricks is
port(
  clk        : in std_logic;
  reset_l    : in std_logic;
  ball_is    : in std_logic := '0';
  ball_x     : in natural := 0;
  ball_y     : in natural := 0;
  x          : in unsigned(9 downto 0);
  y          : in unsigned(8 downto 0);
  is_brick   : out std_logic := '0';
  brick_edge : out edge_type;
  x_out      : out natural := 0;
  y_out      : out natural := 0;
  r          : out std_logic_vector(3 downto 0);
  g          : out std_logic_vector(3 downto 0);
  b          : out std_logic_vector(3 downto 0);
  ballJClk   : out natural
);
end Bricks;

architecture Behavioral of Bricks is
  type BrickMap is array(0 to 29) of std_logic_vector(0 to 43);
  constant brick_reset : BrickMap := (
    x"FFFFFFFFFF0",  x"FFFFFFFFFF8",  x"FFFFFFFFFF0",
    x"FFFFFFFFFF8",  x"FFFFFFFFFF0",  x"FFFFFFFFFF8",
    x"FFFFFFFFFF0",  x"FFFFFFFFFF8",  x"FFFFFFFFFF0",
    x"FFFFFFFFFF8",  x"FFFFFFFFFF0",  x"FFFFFFFFFF8",
    x"FFFFFFFFFF0",  x"FFFFFFFFFF8",  x"FFFFFFFFFF0",
    x"FFFFFFFFFF8",  x"FFFFFFFFFF0",  x"FFFFFFFFFF8",
    x"FFFFFFFFFF0",  x"FFFFFFFFFF8",  x"FFFFFFFFFF0",
    x"FFFFFFFFFF8",  x"FFFFFFFFFF0",  x"FFFFFFFFFF8",
    x"FFFFFFFFFF0",  x"FFFFFFFFFF8",  x"FFFFFFFFFF0",
    x"FFFFFFFFFF8",  x"FFFFFFFFFF0",  x"FFFFFFFFFF8"
  );
  signal brick_map                  : BrickMap := brick_reset;
  signal i, j, ballI, ballJ, ballX  : natural;
  signal brickX, brickY             : natural;
  --signal ballJClk                   : natural;
  signal rX                         : unsigned(10 downto 0);

begin
  rX <= ('0'&x) when j mod 2 = 0 else (('0'&x)+8);
  i <= to_integer(rX(10 downto 4));
  j <= to_integer(y(8 downto 3));

  ballX <= ball_x when ballJ mod 2 = 0 else ball_x+8;
  ballI <= to_integer(to_unsigned(ballX, 10)(9 downto 4));
  ballJ <= to_integer(to_unsigned(ball_y, 10)(9 downto 3));

  brickX <= ballX mod 16;
  brickY <= ball_y mod 8;

  EdgeProc : process ( brickX, brickY ) is
  begin
    if brickX < 2 and brickY < 2 then
      brick_edge <= topleft;
    elsif brickX > 13 and brickY < 2 then
      brick_edge <= topright;
    elsif brickX < 2 and brickY > 5 then
      brick_edge <= bottomleft;
    elsif brickX > 13 and brickY > 5 then
      brick_edge <= bottomright;
    elsif brickX < 2 then
      brick_edge <= left;
    elsif brickX > 13 then
      brick_edge <= right;
    elsif brickY < 2 then
      brick_edge <= top;
    elsif brickY > 5 then
      brick_edge <= bottom;
    else
      brick_edge <= none;
    end if;
  end process;

  BallProc : process (clk, reset_l) is
  begin
    if reset_l='0' then
      brick_map <= brick_reset;
      x_out <= 0;
      y_out <= 0;
      is_brick <= '0';
      ballJClk <= 0;
    elsif rising_edge(clk) then
      x_out <= ball_x;
      y_out <= ball_y;
      ballJClk <= ballJ;
      if ballI >= 0 and ballI < 41 and ballJ >= 0 and ballJ < 30 then
        if ball_is = '1' and brick_map(ballJ)(ballI) = '1' then
          is_brick <= '1';
          brick_map(ballJ)(ballI) <= '0';
        else
          is_brick <= '0';
          brick_map(ballJ)(ballI) <= brick_map(ballJ)(ballI);
        end if;
      end if;
    end if;
  end process;

  PixelColorProc : process (rX, x, y, i, j, brick_map) is
  begin
    if i >= 0 and i < 41 and j >= 0 and j < 30 then
      if brick_map(j)(i) = '1' then
        if rX(3 downto 0)=x"0" or y(2 downto 0)="000" or x=0 or x=639 then
          r <= x"F";
          g <= x"F";
          b <= x"F";
        else
          r <= x"8";
          g <= x"3";
          b <= x"3";
        end if;
      else
        r <= x"0";
        g <= x"0";
        b <= x"0";
      end if;
    else
      r <= x"0";
      g <= x"0";
      b <= x"0";
    end if;
  end process;

end Behavioral;
