library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity GameRunner is
port(
	MAX10_CLK1_50 : in std_logic;
	KEY           : in std_logic_vector(1 downto 0);
	ARDUINO_0     : in std_logic;
	VGA_B         : out std_logic_vector(3 downto 0);
	VGA_G         : out std_logic_vector(3 downto 0);
	VGA_R         : out std_logic_vector(3 downto 0);
	VGA_HS        : out std_logic;
	VGA_VS        : out std_logic
);
end GameRunner;

architecture Behavioral of GameRunner is
  component BrickBreaker is
  port(
  	MAX10_CLK1_50 : in std_logic;
  	VGA_CLK_25    : in std_logic;
  	ADC_CLK_10    : in std_logic;
  	CLK_LOCKED    : in std_logic;
  	KEY           : in std_logic_vector(1 downto 0);
		ARDUINO_0     : in std_logic;
  	VGA_B         : out std_logic_vector(3 downto 0);
  	VGA_G         : out std_logic_vector(3 downto 0);
  	VGA_R         : out std_logic_vector(3 downto 0);
  	VGA_HS        : out std_logic;
  	VGA_VS        : out std_logic
  );
  end component BrickBreaker;
  component VGAClock port
  	(
  		areset		: IN STD_LOGIC  := '0';
  		inclk0		: IN STD_LOGIC  := '0';
  		c0		    : OUT STD_LOGIC ;
  		c1		    : OUT STD_LOGIC ;
  		locked		: OUT STD_LOGIC
  	);
  end component VGAClock;

	signal vga_clk, adc_clk, locked: std_logic := '0';
begin
  vgaClk_inst : VGAClock port map	(
    inclk0 => MAX10_CLK1_50, c0	=> vga_clk, c1 => adc_clk, locked => locked );
  brickBreaker_inst : BrickBreaker port map	(
    MAX10_CLK1_50 => MAX10_CLK1_50,
    VGA_CLK_25 => vga_clk,
    ADC_CLK_10 => adc_clk,
	 	CLK_LOCKED => locked,
    KEY => KEY,
		ARDUINO_0 => ARDUINO_0,
    VGA_B => VGA_B,
    VGA_G => VGA_G,
    VGA_R => VGA_R,
    VGA_HS => VGA_HS,
    VGA_VS => VGA_VS
  );

end Behavioral;
