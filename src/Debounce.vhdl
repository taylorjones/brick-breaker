library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity Debounce is
port(
	clk : in std_logic;
  reset_l : in std_logic;
	btn_in : in std_logic;
	btn_out : out std_logic
);
end Debounce;

architecture Behavioral of Debounce is

  type deb_state_type is (init, idle, btn_deb, add);
	signal current_debstate, next_debstate : deb_state_type;
  signal deb, next_deb : natural;
begin

  btn_out <= '1' when current_debstate=add else '0';

  StateProc : process ( clk, reset_l ) is
  begin
    if reset_l = '0' then
      deb <= 0;
      current_debstate <= init;
    else
      if rising_edge( clk ) then
        deb <= next_deb;
        current_debstate <= next_debstate;
      end if;
    end if;
  end process;

  DebProc : process ( current_debstate, deb, btn_in ) is
  begin
    case current_debstate is
      when init =>
        next_deb <= 0;
        next_debstate <= idle;
      when idle =>
        next_deb <= 0;
        if btn_in = '0' then
          next_debstate <= btn_deb;
        else
          next_debstate <= idle;
        end if;
      when btn_deb =>
        next_deb <= deb + 1;
        if btn_in = '0' then
          next_debstate <= btn_deb;
        else
          if deb < 500000 then
            next_debstate <= idle;
          else
            next_debstate <= add;
          end if;
        end if;
      when add =>
        next_debstate <= idle;
        next_deb <= deb;
      end case;
  end process;
end Behavioral;
