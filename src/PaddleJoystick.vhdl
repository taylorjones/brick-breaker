library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity PaddleJoystick is
port(
  clk     : in std_logic;
  adc_clk : in std_logic;
  locked  : in std_logic;
  reset_l : in std_logic;
  x       : out natural
);
end PaddleJoystick;

architecture Behavioral of PaddleJoystick is
  component JoystickADC is
    port (
      adc_pll_clock_clk      : in  std_logic                     := 'X';             -- clk
      adc_pll_locked_export  : in  std_logic                     := 'X';             -- export
      clock_clk              : in  std_logic                     := 'X';             -- clk
      command_valid          : in  std_logic                     := 'X';             -- valid
      command_channel        : in  std_logic_vector(4 downto 0)  := (others => 'X'); -- channel
      command_startofpacket  : in  std_logic                     := 'X';             -- startofpacket
      command_endofpacket    : in  std_logic                     := 'X';             -- endofpacket
      command_ready          : out std_logic;                                        -- ready
      reset_sink_reset_n     : in  std_logic                     := 'X';             -- reset_n
      response_valid         : out std_logic;                                        -- valid
      response_channel       : out std_logic_vector(4 downto 0);                     -- channel
      response_data          : out std_logic_vector(11 downto 0);                    -- data
      response_startofpacket : out std_logic;                                        -- startofpacket
      response_endofpacket   : out std_logic                                         -- endofpacket
    );
  end component JoystickADC;
  signal com_rdy, rsp_vld    : std_logic;
  signal response_data       : std_logic_vector(11 downto 0);
  signal speed, next_x       : integer;
  signal m_x                 : natural                        := 320;
  signal count, next_count   : natural;
begin

  adc_inst : JoystickADC
    port map (
			clock_clk              => clk,              --  clock.clk
			reset_sink_reset_n     => '1',              --  reset_sink.reset_n
			adc_pll_clock_clk      => adc_clk,          --  adc_pll_clock.clk
			adc_pll_locked_export  => locked,       --  adc_pll_locked.export
			command_valid          => '1',              --  command.valid
			command_channel        => "00001",          --  .channel
			command_startofpacket  => '1',              --  .startofpacket
			command_endofpacket    => '1',              --  .endofpacket
			command_ready          => com_rdy,          --  .ready
			response_valid         => rsp_vld,          --  response.valid
			response_data          => response_data     --  .data
    );

    AdcProc : process (clk) is
    begin
      if rising_edge(clk) and rsp_vld = '1' then
        speed <= -(to_integer(unsigned(response_data(11 downto 7))) - 16);
      end if;
    end process;

    next_x <= m_x + speed when speed > 2 or speed < -2 else m_x;
    x <= m_x;
    next_count <= count+1;
    ClkProc : process (clk, reset_l) is
    begin
      if reset_l = '0' then
        m_x <= 320;
        count <= 0;
      elsif rising_edge(clk) then
        if count > 1200000 then
          count <= 0;
          if next_x > 619 then
            m_x <= 619;
          elsif next_x < 20 then
            m_x <= 20;
          else
            m_x <= next_X;
          end if;
        else
          count <= next_count;
        end if;
      end if;
    end process;

end Behavioral;
