library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity Paddle is
port(
  clk        : in std_logic;
  adc_clk    : in std_logic;
  locked     : in std_logic;
  reset_l    : in std_logic;
  x          : in natural;
  y          : in natural;
  ball_x     : in natural;
  ball_y     : in natural;
  ball_is    : in std_logic;
  is_paddle  : out std_logic;
  paddle_x   : out integer;
  r          : out std_logic_vector(3 downto 0);
  g          : out std_logic_vector(3 downto 0);
  b          : out std_logic_vector(3 downto 0)
);
end Paddle;

architecture Behavioral of Paddle is

  component PaddleJoystick is
  port(
    clk     : in std_logic;
    adc_clk : in std_logic;
    locked  : in std_logic;
    reset_l : in std_logic;
    x       : out natural
  );
  end component PaddleJoystick;
  signal paddleX : natural;
begin

  joystick_inst : PaddleJoystick port map (
    clk => clk, adc_clk => adc_clk, locked => locked, reset_l => reset_l, x => paddleX );

  BallProc : process (clk, reset_l) is
  begin
    if reset_l = '0' then
      is_paddle <= '0';
      paddle_x <= 0;
    elsif rising_edge(clk) then
      if (ball_y<474 or ball_x>paddleX+20 or ball_x<paddleX-20) or ball_is='0' then
        is_paddle <= '0';
        paddle_x <= 0;
      else
        is_paddle <= '1';
        paddle_x <= ball_x - paddleX;
      end if;
    end if;
  end process;

  PixelColorProc : process (x, y, paddleX) is
  begin
    if y<474 or x>paddleX+20 or x<paddleX-20 then
      r <= x"0";
      g <= x"0";
      b <= x"0";
    else
      r <= x"6";
      g <= x"3";
      b <= x"0";
    end if;
  end process;

end Behavioral;
