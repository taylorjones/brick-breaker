library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity VGAClk_TB is
end entity VGAClk_TB;

architecture Behavioral of VGAClk_TB is
  component VGAClock port
  	(
  		areset		: IN STD_LOGIC  := '0';
  		inclk0		: IN STD_LOGIC  := '0';
  		c0		: OUT STD_LOGIC ;
  		locked		: OUT STD_LOGIC
  	);
  end component VGAClock;

  signal inClk : std_logic := '0';
  signal clk : std_logic;
  signal locked : std_logic;

begin
  uut : VGAClock
    port map (
  		areset => '0',
  		inclk0 => inClk,
  		c0 => clk,
  		locked => locked
    );
    clk_process : process
    begin
      inClk <= '0';
      wait for 10 ns;
      inClk <= '1';
      wait for 10 ns;
    end process clk_process;
end architecture Behavioral;
