library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.brick_types.all;

entity BrickBreaker is
port(
	MAX10_CLK1_50 : in std_logic;
	VGA_CLK_25    : in std_logic;
	ADC_CLK_10    : in std_logic;
	CLK_LOCKED    : in std_logic;
	KEY           : in std_logic_vector(1 downto 0);
	ARDUINO_0     : in std_logic;
	VGA_B         : out std_logic_vector(3 downto 0);
	VGA_G         : out std_logic_vector(3 downto 0);
	VGA_R         : out std_logic_vector(3 downto 0);
	VGA_HS        : out std_logic;
	VGA_VS        : out std_logic
);
end BrickBreaker;


architecture Behavioral of BrickBreaker is
  component VGA port
    (
	  	clk        : in std_logic;
	  	clk_locked : in std_logic;
	  	reset_l    : in std_logic;
	  	r          : in std_logic_vector(3 downto 0);
	  	g          : in std_logic_vector(3 downto 0);
	  	b          : in std_logic_vector(3 downto 0);
	  	r_out      : out std_logic_vector(3 downto 0);
	  	g_out      : out std_logic_vector(3 downto 0);
	  	b_out      : out std_logic_vector(3 downto 0);
	  	h_sync     : out std_logic;
	  	v_sync     : out std_logic;
	  	x          : out natural;
	  	y          : out natural;
	  	blank      : out std_logic
    );
  end component VGA;
	component Bricks port
		(
		  clk        : in std_logic;
		  reset_l    : in std_logic;
		  ball_is    : in std_logic := '0';
		  ball_x     : in natural := 0;
		  ball_y     : in natural := 0;
		  x          : in unsigned(9 downto 0);
		  y          : in unsigned(8 downto 0);
		  is_brick   : out std_logic := '0';
		  brick_edge : out edge_type;
		  x_out      : out natural := 0;
		  y_out      : out natural := 0;
		  r          : out std_logic_vector(3 downto 0);
		  g          : out std_logic_vector(3 downto 0);
		  b          : out std_logic_vector(3 downto 0);
		  ballJClk   : out natural
		);
	end component Bricks;
	component Ball port
		(
			clk        : in std_logic;
			reset_l    : in std_logic;
		  go         : in std_logic;
			x          : in natural;
			y          : in natural;
		  brick_is   : in std_logic;
		  brick_edge : in edge_type;
		  brick_x    : in natural;
		  brick_y    : in natural;
		  paddle_is  : in std_logic;
		  paddle_x   : in integer;
			x_out      : out natural;
			y_out      : out natural;
		  is_ball    : out std_logic;
			r          : out std_logic_vector(3 downto 0);
			g          : out std_logic_vector(3 downto 0);
			b          : out std_logic_vector(3 downto 0)
		);
	end component Ball;
	component Paddle is
	port(
	  clk       : in std_logic;
	  adc_clk   : in std_logic;
	  locked    : in std_logic;
	  reset_l   : in std_logic;
	  x         : in natural;
	  y         : in natural;
	  ball_x    : in natural;
	  ball_y    : in natural;
	  ball_is   : in std_logic;
	  is_paddle : out std_logic;
	  paddle_x  : out integer;
	  r         : out std_logic_vector(3 downto 0);
	  g         : out std_logic_vector(3 downto 0);
	  b         : out std_logic_vector(3 downto 0)
	);
	end component Paddle;
	component Debounce is
	port(
		clk : in std_logic;
	  reset_l : in std_logic;
		btn_in : in std_logic;
		btn_out : out std_logic
	);
	end component;

  signal x, y, ball_x, ball_y, brick_x, brick_y : natural := 0;
  signal ux: unsigned(9 downto 0) := (others => '0');
  signal uy: unsigned(8 downto 0) := (others => '0');
  signal blank: std_logic := '0';
  signal r, g, b : std_logic_vector(3 downto 0) := (others => '0');

	signal ball_is, brick_is, paddle_is : std_logic := '0';
	signal brick_edge : edge_type;
	signal paddle_x : integer;
	signal reset_l, go, go_deb : std_logic := '0';
	signal r_bricks, g_bricks, b_bricks : std_logic_vector(3 downto 0) := (others => '0');
	signal r_ball, g_ball, b_ball : std_logic_vector(3 downto 0) := (others => '0');
	signal r_paddle, g_paddle, b_paddle : std_logic_vector(3 downto 0) := (others => '0');
	signal ballJClk : natural;
begin
  ux <= to_unsigned(x, 10);
  uy <= to_unsigned(y, 9);

  vga_inst : VGA port map	(
    clk => VGA_CLK_25,
    clk_locked => CLK_LOCKED,
    reset_l => reset_l,
    r => r,
    g => g,
    b => b,
    r_out => VGA_R,
    g_out => VGA_G,
    b_out => VGA_B,
    h_sync => VGA_HS,
    v_sync =>VGA_VS,
    x => x,
    y => y,
    blank => blank
  );
	brick_inst : Bricks port map	(
		clk => MAX10_CLK1_50,
		reset_l => reset_l,
		ball_is => ball_is,
		ball_x => ball_x,
		ball_y => ball_y,
		x => ux,
		y => uy,
		x_out => brick_x,
		y_out => brick_y,
		is_brick => brick_is,
		brick_edge => brick_edge,
		r => r_bricks,
		g => g_bricks,
		b => b_bricks,
		ballJClk => ballJClk
	);
	ball_inst : Ball port map	(
		clk => MAX10_CLK1_50,
		reset_l => reset_l,
		go => go_deb,
		brick_is => brick_is,
		brick_edge => brick_edge,
		brick_x => brick_x,
		brick_y => brick_y,
		paddle_is => paddle_is,
		paddle_x => paddle_x,
		x => x,
		y => y,
		x_out => ball_x,
		y_out => ball_y,
		is_ball => ball_is,
		r => r_ball,
		g => g_ball,
		b => b_ball
	);
	paddle_inst : Paddle port map	(
		clk => MAX10_CLK1_50,
		adc_clk => ADC_CLK_10,
		locked => CLK_LOCKED,
		reset_l => reset_l,
		is_paddle => paddle_is,
		paddle_x => paddle_x,
		x => x,
		y => y,
		ball_is => ball_is,
		ball_x => ball_x,
		ball_y => ball_y,
		r => r_paddle,
		g => g_paddle,
		b => b_paddle
	);
	deb_inst: Debounce port map (
		clk => MAX10_CLK1_50,
	  reset_l => reset_l,
		btn_in => go,
		btn_out => go_deb
	);
  reset_l <= CLK_LOCKED and KEY(0);
  go <= (not ARDUINO_0) or (not KEY(1));
	r <= r_bricks or r_paddle or r_ball;
	g <= g_bricks or g_paddle or g_ball;
	b <= b_bricks or b_paddle or b_ball;

end Behavioral;
