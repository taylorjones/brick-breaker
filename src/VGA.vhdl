library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity VGA is
port(
	clk : in std_logic;
	clk_locked : in std_logic;
	reset_l : in std_logic;
	r : in std_logic_vector(3 downto 0);
	g : in std_logic_vector(3 downto 0);
	b : in std_logic_vector(3 downto 0);
	r_out : out std_logic_vector(3 downto 0);
	g_out : out std_logic_vector(3 downto 0);
	b_out : out std_logic_vector(3 downto 0);
	h_sync : out std_logic;
	v_sync : out std_logic;
	x : out natural;
	y : out natural;
	blank : out std_logic
);
end VGA;


architecture Behavioral of VGA is

  signal hcnt, next_hcnt: natural := 1;
  signal vcnt, next_vcnt: natural := 1;
  signal m_y, next_y: natural := 0;
	signal hblank, vblank, mblank: std_logic := '0';
	signal m_reset_l: std_logic := '0';

	type sync_state_type is (init, front, pulse, back, pixels);
	signal current_hstate, next_hstate, current_vstate, next_vstate : sync_state_type;
begin
			x <= (hcnt-1) when current_hstate = pixels and current_vstate = pixels and hcnt > 0 else 0;
			y <= (m_y-1) when current_hstate = pixels and current_vstate = pixels and m_y > 0 else 0;
			mblank <= vblank or hblank;
			blank <= mblank;
			m_reset_l <= (reset_l and clk_locked);

			StateProc : process ( clk, m_reset_l ) is
			begin
				if m_reset_l='0' then
					hcnt <= 1;
					vcnt <= 1;
					current_hstate <= init;
					current_vstate <= init;
					m_y <= 1;
				else
			  	if (rising_edge(clk)) then
						hcnt <= next_hcnt;
						vcnt <= next_vcnt;
		    		current_hstate <= next_hstate;
		    		current_vstate <= next_vstate;
						m_y <= next_y;
			  	end if;
				end if;
			end process;

			PixelColorProc : process (mblank, r, g, b) is
			begin
				if mblank = '1' then
					r_out <= (others => '0');
					b_out <= (others =>'0');
					g_out <= (others => '0');
				else
					r_out <= r;
					g_out <= g;
					b_out <= b;
				end if;
			end process;

			HorStateProc : process ( current_hstate, current_vstate, hcnt, m_y ) is
			begin
		  	case current_hstate is
					when init =>
						next_y  <= 1;
						h_sync <= '1';
						hblank <= '1';
						next_hcnt <= 1;
						next_hstate <= front;
					when front =>
						next_y  <= m_y;
						h_sync <= '1';
						hblank <= '1';
						next_hcnt <= hcnt+1;
						if hcnt >= 16 then
							next_hcnt <= 1;
							next_hstate <= pulse;
						else
							next_hcnt <= hcnt+1;
							next_hstate <= front;
						end if;
					when pulse =>
						next_y  <= m_y;
						h_sync <= '0';
						hblank <= '1';
						if hcnt >= 96 then
							next_hcnt <= 1;
							next_hstate <= back;
						else
							next_hcnt <= hcnt+1;
							next_hstate <= pulse;
						end if;
					when back =>
						next_y  <= m_y;
						h_sync <= '1';
						hblank <= '1';
						if hcnt >= 48 then
							next_hcnt <= 1;
							next_hstate <= pixels;
						else
							next_hcnt <= hcnt+1;
							next_hstate <= back;
						end if;
					when pixels =>
						h_sync <= '1';
						hblank <= '0';
						if hcnt >= 640 then
							next_hcnt <= 1;
							if current_vstate=pixels then
								next_y  <= m_y+1;
							else
								next_y  <= 1;
							end if;
							next_hstate <= front;
						else
							next_hcnt <= hcnt+1;
							next_y  <= m_y;
							next_hstate <= pixels;
						end if;
					end case;
			end process;

			VertStateProc : process ( current_vstate, vcnt ) is
			begin
		  	case current_vstate is
					when init =>
						v_sync <= '1';
						vblank <= '1';
						next_vcnt <= 1;
						next_vstate <= front;
					when front =>
						v_sync <= '1';
						vblank <= '1';
						next_vcnt <= vcnt+1;
						if vcnt >= 8000 then
							next_vcnt <= 1;
							next_vstate <= pulse;
						else
							next_vcnt <= vcnt+1;
							next_vstate <= front;
						end if;
					when pulse =>
						v_sync <= '0';
						vblank <= '1';
						if vcnt >= 1600 then
							next_vcnt <= 1;
							next_vstate <= back;
						else
							next_vcnt <= vcnt+1;
							next_vstate <= pulse;
						end if;
					when back =>
						v_sync <= '1';
						vblank <= '1';
						if vcnt >= 26400 then
							next_vcnt <= 1;
							next_vstate <= pixels;
						else
							next_vcnt <= vcnt+1;
							next_vstate <= back;
						end if;
					when pixels =>
						v_sync <= '1';
						vblank <= '0';
						if vcnt >= 384000 then
							next_vcnt <= 1;
							next_vstate <= front;
						else
							next_vcnt <= vcnt+1;
							next_vstate <= pixels;
						end if;
					end case;
			end process;
end Behavioral;
